const db = require('../../config/db');

// EMPTY OBJECT
// USED FOR EXPORTING THE FUNCTIONS BELOW
const Repo = {};

// CREATE REPO
Repo.create = (name, description) => {
  return db.none(`INSERT into data_sets(name, description)` + `VALUES($1, $2)`, [name, description]);
};

// GET ALL REPOS
Repo.get = () => {
  return db.any('SELECT id, name, description FROM data_sets');
};

// UPDATE REPO
Repo.update = (name, desc, id) => {
  return db.none(`UPDATE data_sets SET name = $1, description = $2 WHERE id = $3`, [
    name,
    desc,
    id
  ]);
};

// DELETE REPO
Repo.delete = id => {
  return db.none(`DELETE from data_sets WHERE id = $1`, id);
};

module.exports = Repo;
