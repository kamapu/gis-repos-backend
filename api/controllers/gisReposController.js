// REQUIRE MODEL
const Repo = require('../models/gisReposModel');

module.exports = {
  // GET ALL REPOS
  getRepos(req, res, next) {
    Repo.get()
      .then(data => res.status(200).json({ success: true, repositories: data }))
      .catch(err => res.status(400).json({ err }));
  },

  // CREATE REPO
  createRepo(req, res, next) {
    // USE BODY PARSER TO EXTRACT DATA FROM CLIENT
    const { name, description } = req.body;

    Repo.create(name, description)
      .then(() => res.status(201).json({ success: true, msg: 'Repository created' }))
      .catch(err => res.status(400).json({ err }));
  },

  // UPDATE REPO
  updateRepo(req, res, next) {
    // USE BODY PARSER TO EXTRACT DATA FROM CLIENT
    const { name, description } = req.body;
    // ID OF REPO TO UPDATE
    let id = req.params.id;

    Repo.update(name, description, id)
      .then(() => res.status(200).json({ success: true, msg: `Repository #${id} updated` }))
      .catch(err => res.status(400).json({ err }));
  },

  // DELETE REPO
  deleteRepo(req, res, next) {
    let id = req.params.id;

    Repo.delete(id)
      .then(() => res.status(200).json({ success: true, msg: `Repository #${id} deleted` }))
      .catch(err => res.status(400).json({ err }));
  }
};
