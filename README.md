# gis-repos-backend

A backend application to manage a collection of repositories for [GIS](https://en.wikipedia.org/wiki/Geographic_information_system) data sets.

## Get started

### Requirements

To work on the project the following tools are required:
  - A Postgresql instance is required for data storage.
    - Install [docker](https://docs.docker.com/engine/install/) and [docker-compose](https://docs.docker.com/compose/install/) to start a local Postgresql database.

### Initialize a local runtime environment

With Docker you can start a local development environment:

```bash
$ docker-compose up --build -d
Building app
Step 1/19 : FROM node:current
...
```

Make the first access to the API.

```bash
# API Entrypoint
$ curl http://localhost:3000
{
  "links": {
    "repositories": "http://localhost:3000/repositories"
  }
}

# List all repositoires
$ curl http://localhost:3000/repositories
{
  "success": true,
  "repositories": [
    {
      "id": 1,
      "name": "NAME A",
      ...
    },
    {
      "id": 2,
      "name": "NAME B",
      ...
    },
    ...
  ]
}
```
