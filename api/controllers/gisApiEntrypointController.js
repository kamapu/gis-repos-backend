const apiBaseURL = process.env.API_PUBLIC_BASE_URL || 'http://localhost:3000';

module.exports = {
  // SHOW ENDPOINTS
  showEndpoints(req, res, next) {
    res.status(200).json({ "links": { "repositories": apiBaseURL + '/repositories' } });
  },
};
