COPY public.data_sets (id, name, description, "timestamp") FROM stdin;
1	AmeriGEO	Collection of data focusing on the American continent.	2020-10-23
2	AQUASTAT	Information on water at the Food and Agriculture Organization of the United Nations (FAO).	2020-10-23
3	Aster- NASA and Japan	30 m Grid	2020-10-23
4	CHELSA	Climatic model at a resolution of 30 arc seconds done on the basis of ERA-Interim climatic reanalysis. The model and data are described by [Karger et al. (2017)](http://doi.org/10.1038/sdata.2017.122).	2020-10-23
5	Climatic Research Unit Data	Global climate data (grid). Includes Google Earth Interface for temperature and precipitation data.	2020-10-23
6	Conservation GIS Data	Maps of Ecoregions offered by the Nature Conservancy.	2020-10-23
7	Conservation Science Data and Tools	Set of tools and data associated to diversity conservation at the World Wildlife Fund (WWF).	2020-10-23
8	Copernicus Open Access Hub	Access to ESA Sentinel 1, 2, and 3 data sets.	2020-10-23
9	CRU Hulme Global Land Precipitation Data	Not yet described.	2020-10-23
10	DHI	Dynamic Habitat Indices.	2020-10-23
11	DIVA GIS	Collection of sources of data. Read documentation to access data	2020-10-23
12	EEA	Not yet described.	2020-10-23
13	Environmental Data Explorer	Environmental data by the United Nations Environment Programme (UNEP).	2020-10-23
14	European and Climate assessment and dataset	Not yet described.	2020-10-23
15	European and Climate assessment and dataset E-OBS gridded dataset	Not yet described.	2020-10-23
16	European Environment Agency	Corine Land Cover changes from 2000 to 2006 based on validated Corine Land Cover data	2020-10-23
17	European Reanalysis and Observations for Monitoring	Not yet described.	2020-10-23
18	European Space Agency	25 m Grid	2020-10-23
19	European Space Agency (dupl)	Not yet described.	2020-10-23
20	European Space Agency GlobCover Portal	Not yet described.	2020-10-23
21	Federal office of Meteorology and climatology MeteoSwiss	Not yet described.	2020-10-23
22	Free GIS Data	Personal link collection by Robin Wilson. Including data sets for human and physical geography and data sets for individual countries/areas.	2020-10-23
23	GEOSS Portal	Global GIS database from the European Space Agency.	2020-10-23
24	GISGeography	Alternative for accessing GIS data sets. Including brief tutorials on how to download data sets.	2020-10-23
25	Global Administrative Areas	Administrative units worldwide.	2020-10-23
26	Global Land Cover-SHARE (GLC-SHARE)- FAO	centralized database of land use / cover	2020-10-23
27	Global Runoff Data Base	Runoff data from the Global Runoff Data Centre (GRDC).	2020-10-23
28	Global Runoff Data Centre	Not yet described.	2020-10-23
29	Global Surface Water Explorer	Global or 10$^{\\circ}$x10$^{\\circ}$ tiles Landsat imagery covering location and temporal distribution of water surfaces over the past 32 years, including information on occurrence, change, seasonality, recurrence, transitions and maximum extent.	2020-10-23
30	GlobCover	Worldwide information on land cover features. Land cover maps available for periods December 2004 - June 2006 and January - December 2009.	2020-10-23
31	GLOBELAND30	Global land cover data (30 m spatial resolution).	2020-10-23
32	Government of South Australia	Not yet described.	2020-10-23
33	Harmonized World Soil Database	Spatial data of soil classification with worldwide coverage. Including supplementary data like terrain and land cover data.	2020-10-23
34	Humanitarian Data Exchange	Data required for humanitarian purposes.	2020-10-23
35	HydroSHEDS	Hydrological data and maps based on Shuttle Elevation Derivatives at multiple scales.	2020-10-23
36	HydroSHEDS (Hydrological data and maps based on SHuttle Elevation Derivatives at multiple Scales)- USGS	a suite of geo-referenced data sets (vector and raster) at various scales, including river networks, watershed boundaries, drainage directions, and flow accumulations	2020-10-23
37	International Soil Moisture Network	Not yet described.	2020-10-23
38	IRI/LDEO Climate Data Library	Collection of sources of data	2020-10-23
39	IRI/LDEO Climate Data Library (dupl)	Collection of sources of data	2020-10-23
40	ISRIC - World Soil Information	Not yet described.	2020-10-23
41	ISRIC World Soil Information	Access to soil profiles and soil grids at the International Soil Reference and Information Centre (ISRIC).	2020-10-23
42	Map East Africa	Collection of GIS information (shapefiles and ArcGIS layer packages) for East African countries with a social and policitcal focus on Sudan and South Sundan, including a conflict mapping approach.	2020-10-23
43	MapCruzin	Free GIS maps, ArcGIS shapefiles and geospatial data.	2020-10-23
44	Met Office UK	Not yet described.	2020-10-23
45	Met Office UK (dupl)	Not yet described.	2020-10-23
46	MultiPlans	Collection of data provider	2020-10-23
47	NASA Earth Observations	NASA Earth Observations (NEO) data sets of various categories.	2020-10-23
48	NASA Global Change Master Directory	Data sets of various global change related topics.	2020-10-23
49	NASA TRMM	Not yet described.	2020-10-23
50	Natural Earth	Global physical and cultural/political information in three different scales.	2020-10-23
51	NOAA California Nevada River Forecast Center	Not yet described.	2020-10-23
52	NOAA-Earth System Research Laboratory	Not yet described.	2020-10-23
53	NOAA-NWS	Not yet described.	2020-10-23
54	Office of Public work	Not yet described.	2020-10-23
55	openAFRICA	Volunteer driven open data platform for Africa.	2020-10-23
56	OpenStreetMap	Open access to vectors of roads and further features.	2020-10-23
57	Potential Natural Vegetation of Eastern Africa	Maps in various formats of the VECEA project (Vegetaion and Climate Change in Eastern Africa) for eight eastern and southern African countries.	2020-10-23
58	Remote Sensing Biodiversity	Community webpage for remote sensing, biodiversity and conservation. Contains GIS data, terrestrial and marine remote sensing data and visualisation, software and training resources.	2020-10-23
59	SRTM 90m Digital Elevation Data	Digital elevation data from the Shuttle Radar Topography Mission (SRTM).	2020-10-23
60	SRTM- NASA	90 m Grid	2020-10-23
61	TEOW	Terrestrial Ecoregions of the world.	2020-10-23
62	Topographisches Informationsmanagement Nordrhein-Westfalen	Not yet described.	2020-10-23
63	UNEP Environmental Data Explorer	Collection of sources of data	2020-10-23
64	University of East Anglia Climatic Research Unit	Not yet described.	2020-10-23
65	US Department of Agriculture	Not yet described.	2020-10-23
66	USGS	Data provided by the United States Geological Survey (USGS), including various topics.	2020-10-23
67	USGS Land Cover Institute	Not yet described.	2020-10-23
68	USGS LandSat	Not yet described.	2020-10-23
69	WATCH	Data sets for regional and global studies on climate and water. Accessible via CEH (Centre for Ecology and Hydrology) gateway catalogue. Including maps displaying WATCH topics "Rainfall", "Freshwater", "Evaporation" and "Land".	2020-10-23
70	Web page collection of data provider	Not yet described.	2020-10-23
71	Webpage of collection of database	Not yet described.	2020-10-23
72	WETechData	Climate Change Data (optimized for SWAT model).	2020-10-23
73	wikiloc	Repository for upload and download of trachlogs (requires registration).	2020-10-23
74	World Cities Database	Table with population of cities in the world including their coordinates.	2020-10-23
75	World Resources Institute	Worldwide data sets sorted by topics at the World Resource Institute (WRI).	2020-10-23
76	WorldClim	Results of the WroldClim model for global climate data. The first version is documented in [Hijmans et al. (2005)](http://doi.org/10.1002/joc.1276), while a second version is published by [Fick & Hijmans (2017)](http://doi.org/10.1002/joc.5086).	2020-10-23
\.