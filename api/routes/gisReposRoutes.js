// REQUIRE CONTROLLER
const ReposController = require('../controllers/gisReposController');
const ApiInfoController = require('../controllers/gisApiEntrypointController');
const basicAuth = require('../../config/basicAuth')

module.exports = app => {
  app.get('/', ApiInfoController.showEndpoints);
  app.get('/repositories', ReposController.getRepos);
  app.post('/repositories', basicAuth, ReposController.createRepo);
  app.put('/repositories/:id', basicAuth, ReposController.updateRepo);
  app.delete('/repositories/:id', basicAuth, ReposController.deleteRepo);
};
