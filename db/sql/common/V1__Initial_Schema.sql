-- Database is already created
-- Table resides in the public schema

-- Main tables -----------------------------------------------------------------

-- Data Sets
create table data_sets (
	id serial primary key,
	"name" text unique not null,
	description text,
	"timestamp" date
);
comment on table data_sets is 'List of repositories';
comment on column data_sets.id is 'ID of repository entry.';
comment on column data_sets."name" is 'Title of the repository entry.';
comment on column data_sets.description
	is 'Extent description on the content of the repository';
comment on column data_sets."timestamp" is 'Date of last update for the entry.';

-- Links
create table links (
	link_id serial primary key,
	url text unique not null
);
comment on table links is 'List of hyperlinks';
comment on column links.link_id is 'Identifier for the URL.';
comment on column links.url
	is 'Link used to access to one or more repositories.';

-- Keywords
create table keywords (
	kword_id serial primary key,
	kword text unique not null
);
comment on table keywords is 'List of categories (keywords) for repositories.';
comment on column keywords.kword_id is 'Identifier of the category.';
comment on column keywords.kword is 'The name of the category.';

-- Relationships ---------------------------------------------------------------

-- Links
create table fk_links (
	id integer references data_sets (id) not null,
	link_id integer references links (link_id) not null
);
comment on table fk_links is 'Relationship links to repos.';
comment on column fk_links.id is 'Relationship to repo.';
comment on column fk_links.link_id is 'Relationship to link.';

-- Keywords
create table fk_keywords (
	id integer references data_sets (id) not null,
	kword_id integer references keywords (kword_id) not null
);
comment on table fk_keywords is 'Relationship keywords to repos.';
comment on column fk_keywords.id is 'Relationship to repo.';
comment on column fk_keywords.kword_id is 'Relationship to keyrowd.';
