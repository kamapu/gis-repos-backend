const basicAuth = require('express-basic-auth')

var staticUserAuth = basicAuth({
    users: {
        'admin': 'test',
        'user': 'test'
    },
    challenge: true,
    realm: 'GIS Repositories API',
})

module.exports = staticUserAuth;