#!/usr/bin/env bash

# fail on first error
set -e

echo "> Running start script..."

if [ "${FLYWAY_MIGRATE}" == "true" ]
then
    pushd .

    echo "> Wait for database on ${DATABASE_HOST}:${DATABASE_PORT} to be ready..."

    /wait-for-it.sh "${DATABASE_HOST}:${DATABASE_PORT}" --timeout=120

    echo "> database seems to be ready..."

    echo "> Creating flyway config file..."

    cd /flyway/conf/
    {
        echo "flyway.url=$FLYWAY_URL"
        echo "flyway.user=$FLYWAY_USER"
        echo "flyway.password=$FLYWAY_PASSWORD"
    } >> flyway.conf

    # Recommended: create a database backup now!

    echo "> Migrating database with flyway..."

    cd /flyway/
    echo "> Running flyway info before migrate ..."
    /flyway/flyway info
    echo "> Running flyway migrate ..."
    /flyway/flyway migrate
    echo "> Running flyway info after migrate ..."
    /flyway/flyway info

    popd
else
    echo "> Database migration is deactivated (FLYWAY_MIGRATE=${FLYWAY_MIGRATE})"
fi


echo ""
echo "> Running node server.js"
echo ""
node server.js