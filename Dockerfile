FROM node:current

ENV DATABASE_HOST localhost
ENV DATABASE_PORT 5432
ENV DATABASE_NAME dbname

ENV FLYWAY_VERSION 7.9.1
ENV FLYWAY_MIGRATE false
ENV FLYWAY_URL jdbc:postgresql://${DATABASE_HOST}:${DATABASE_PORT}/${DATABASE_NAME}
ENV FLYWAY_USER changeme
ENV FLYWAY_PASSWORD changeme

# Install flyway
RUN wget -q https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/${FLYWAY_VERSION}/flyway-commandline-${FLYWAY_VERSION}-linux-x64.tar.gz \
    && tar zxvf flyway-commandline-${FLYWAY_VERSION}-linux-x64.tar.gz \
    && mkdir /flyway \
    && mv flyway-${FLYWAY_VERSION}/* /flyway \
    && rm -r flyway-${FLYWAY_VERSION}/ flyway-commandline-${FLYWAY_VERSION}-linux-x64.tar.gz

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

# Prepare Flyway SQL Scripts for Migration
RUN rm -rf /flyway/sql \
    && mv db/sql /flyway/sql

EXPOSE 8080

# Copy docker scripts
COPY ./docker/scripts/*.sh /
RUN chmod +x /*.sh

CMD ["/docker-entrypoint.sh"]