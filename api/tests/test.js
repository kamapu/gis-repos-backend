// Import the dependencies for testing
const app = require('../../server');
const supertest = require('supertest');
const { expect } = require('chai');

describe("API Entrypoint", () => {
    describe("GET /", () => {
        it("should get links of primary resources", (done) => {
            supertest(app)
                .get('/')
                .expect(200)
                .end(function(err, res) {
                  expect(res.body).property('links');
                  done();
                });
            });
        });
    });


describe('Create Repository', function() {
    const endpoint = '/repositories'

    it('should reject on missing header', function(done) {
        supertest(app)
            .post(endpoint)
            .expect(401, done)
    })

    it('should reject on wrong credentials', function(done) {
        supertest(app)
            .post(endpoint)
            .auth('darth', 'vader')
            .expect(401, done)
    })

    it('should reject with challenge', function(done) {
        supertest(app)
            .post(endpoint)
            .expect(function (res) {
                if(!res.header['www-authenticate'])
                    throw new Error('Response should have a challenge')
            })
            .expect(401, done)
    })

    it('should accept correct credentials', function(done) {
        supertest(app)
            .post(endpoint)
            .auth('admin', 'test')
            .expect(400, done)
    })
})

describe('Update Repository', function() {
    const endpoint = '/repositories/1'

    it('should reject on missing header', function(done) {
        supertest(app)
            .put(endpoint)
            .expect(401, done)
    })

    it('should reject on wrong credentials', function(done) {
        supertest(app)
            .put(endpoint)
            .auth('darth', 'vader')
            .expect(401, done)
    })

    it('should reject with challenge', function(done) {
        supertest(app)
            .put(endpoint)
            .expect(function (res) {
                if(!res.header['www-authenticate'])
                    throw new Error('Response should have a challenge')
            })
            .expect(401, done)
    })

    it('should accept correct credentials', function(done) {
        supertest(app)
            .put(endpoint)
            .auth('admin', 'test')
            .expect(400, done)
    })
})

describe('Delete Repository', function() {
    const endpoint = '/repositories/1'

    it('should reject on missing header', function(done) {
        supertest(app)
            .delete(endpoint)
            .expect(401, done)
    })

    it('should reject on wrong credentials', function(done) {
        supertest(app)
            .delete(endpoint)
            .auth('darth', 'vader')
            .expect(401, done)
    })

    it('should reject with challenge', function(done) {
        supertest(app)
            .delete(endpoint)
            .expect(function (res) {
                if(!res.header['www-authenticate'])
                    throw new Error('Response should have a challenge')
            })
            .expect(401, done)
    })

    it('should accept correct credentials', function(done) {
        supertest(app)
            .delete(endpoint)
            .auth('admin', 'test')
            .expect(400, done)
    })
})
